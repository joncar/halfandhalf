<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "inicio";
$route['404_override'] = '';
$route['page'] = 'inicio/index/main';
$route['favoritos'] = 'productos/favoritos/list';
$route['favoritos/(:any)'] = 'productos/favoritos';
$route['page/(:any)'] = 'inicio/index/main/$1';
$route['v/(:any)'] = "inicio/index/$1";
$route['a/(:any)'] = "inicio/loadViewAjax/$1";
$route['watch/(:any)'] = "productos/view/$1";
$route['publicar'] = "productos/publicar/add";
$route['publicar/(:any)'] = "productos/publicar/$1";
$route['mis-anuncios'] = "productos/publicar";

/* End of file routes.php */
/* Location: ./application/config/routes.php */