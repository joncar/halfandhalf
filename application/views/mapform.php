<script type="text/javascript"
      src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDBv7rAHZNUTj1V0rLfdTB7gEtcO7rc9QE&sensor=true">
    </script>
    <script type="text/javascript">
      function initialize() {
  var mapOptions = {
    zoom: 4,
    center: new google.maps.LatLng(40.245991504199026, -3.603515625),
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };

  var map = new google.maps.Map(document.getElementById('map_canvas'),
      mapOptions);

  var marker = new google.maps.Marker({
    position: map.getCenter(),
    map: map,
    title: 'Click to zoom'
  });

  google.maps.event.addListener(map, 'click', function(event) {
    marker.setPosition(event.latLng);
    $("#field-coordenadas").val(marker.getPosition());
  });
}

    </script>
<div id="map_canvas" style="width:400px; height:200px"></div>
<?= form_input('coordenadas','','id="field-coordenadas" placeholder="Ubicación" readonly') ?>
<script>initialize();</script>
