      <? 
        $c = str_replace('(','',$producto->coordenadas);
        $c = str_replace(')','',$c);
        list($x,$y) = explode(",",$c);
      ?>
<section class="content">
		<ul class="breadcrumb">
			<li><a href="<?= site_url() ?>">Inicio</a> <span class="divider">/</span></li>
			<li><a href="<?= site_url('?cat='.$producto->categoria) ?>"><?= $producto->cat ?></a> <span class="divider">/</span></li>
			<li><a href="<?= site_url('?cat='.$producto->categoria.'&ciudad='.$producto->ciudad) ?>"><?= $producto->ciud ?></a> <span class="divider">/</span></li>			
			<li><?= $producto->titulo ?></li>
                        <li style="float:right">Anuncio #<?= $producto->id ?></li>
		</ul>
					
	<h1><?= empty($producto->intercambio)?$producto->titulo:$producto->titulo." por ".$producto->intercambio ?></h1>
        <div class="span5">
		    <div id="myCarousel" class="carousel slide">
					<!-- Carousel items -->
				<div class="carousel-inner">
				<? if(!empty($producto->foto1)): ?>
					<div class="active item"><img src="<?= base_url($this->pathPictures."/".$producto->foto1) ?>"></div>
                                <? else: ?>
                                        <div class="active item"><img src="<?= base_url('assets/uploads/pictures/vacio.png') ?>"></div>
				<? endif ?>
				<? if(!empty($producto->foto2)): ?>
					<div class="item"><img src="<?= base_url($this->pathPictures."/".$producto->foto2) ?>"></div>
				<? endif ?>
				<? if(!empty($producto->foto3)): ?>
					<div class="item"><img src="<?= base_url($this->pathPictures."/".$producto->foto3) ?>"></div>
				<? endif ?>
				<? if(!empty($producto->foto4)): ?>
					<div class="item"><img src="<?= base_url($this->pathPictures."/".$producto->foto4) ?>"></div>
				<? endif ?>
				<? if(!empty($producto->foto5)): ?>
					<div class="item"><img src="<?= base_url($this->pathPictures."/".$producto->foto5) ?>"></div>
				<? endif ?>
				<? if(!empty($producto->foto6)): ?>
					<div class="item"><img src="<?= base_url($this->pathPictures."/".$producto->foto6) ?>"></div>
				<? endif ?>
				</div>
				<!-- Carousel nav -->
                                <? if(!empty($producto->foto1) && !empty($producto->foto2)): ?>
				<a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
				<a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
                                <? endif ?>
			</div>
			
		</div>
		<div class="span6 well">
		<h2>Descripción</h2>

		<div class="row-fluid well well-small">
			<div class="span3"><b>Categoria:</b><br/> <?= $producto->cat ?></div>
			<div class="span2"><b>Puntos:</b><br/> <?= $this->main->price($producto->precio) ?></div>
			<div class="span2"><b>Ciudad:</b><br/> <?= $producto->ciud ?></div>
			<div class="span2"><b>Tipo:</b><br/> <?= $producto->tipo ?></div>
			<div class="span2"><b>Publicación:</b><br/> <?= date("d/m/Y H:i",strtotime($producto->fecha)) ?></div>
		</div>
		<div class="row-fluid well well-small">
			<div class="span4"><b>Contacto:</b><br/> <?= $producto->Nombre." ".$producto->Apellido ?></div>
			<div class="span4"><b>Email:</b><br/> <?= $producto->email ?></div>
			<div class="span4"><b>Telefono:</b><br/> <?= $producto->telefono ?> </div>
		</div>
		<? if(!empty($_SESSION['user']) && $this->db->get_where('favoritos',array('producto'=>$producto->id,'user'=>$_SESSION['user']))->num_rows==0): ?>
		<div class="well well-small">
			<a href="javascript:ajax('producto=<?= $producto->id ?>',undefined,undefined,undefined,'<?= base_url('productos/favoritos/insert') ?>')" class="btn btn-info"><i class="icon-white icon-user"></i>+ Favoritos</a>
		</div>
		<? endif ?>
		<div class="well well-small">
			<b>Detalles</b>
			<p>
			<?= $producto->descripcion ?>
		</div>
                
                <div class="well well-small">
			<b>Ubicación en el mapa</b>
			<p>
                        <div id="map"  style="width:400px; height:200px"></div>
                        <a href="javascript:resizemap()">Pantalla completa</a>
		</div>
		</div>
		
		<div class="container span12 well">
			Hazle una pregunta al Anunciante
			<? if(!empty($_SESSION['user'])): ?>
			<div class="alert alert-info">
				<form onsubmit="return send(this,'<?= base_url('productos/preguntas/insert') ?>')">
				<?= form_input('texto','','id="field-texto" placeholder="Preguntar" style="width:100%"'); ?>
				<input type="hidden" name="producto" id="field-producto" value="<?= $producto->id ?>">
				<button class="btn btn-success">Preguntar</button>
				</form>
			</div>
			<? else : ?>
			<div class="alert alert-info">Para hacer una pregunta primero debes <a href="<?= site_url('v/registrar') ?>">registrarte</a> o <a href="<?= site_url('v/conectar'); ?>">Iniciar Sesión</a></div>
			<? endif ?>
			<? if($producto->preguntas): ?>
				<? foreach($producto->preguntas->result() as $pregunta): ?>
					<div class="alert alert-info">
						<?= $pregunta->texto ?>
					</div>
					<? $respuesta = $this->main->getRespuestas($pregunta->id) ?>
					<? if($respuesta): ?>
						<div style="margin-left:20px" class="alert alert-success">
							<?= $respuesta->texto ?>
						</div>
					<? endif ?>
				<? endforeach ?>
			<? endif ?>
			
		</div>
</div>

</section>
<script type="text/javascript"
      src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDBv7rAHZNUTj1V0rLfdTB7gEtcO7rc9QE&sensor=true">
    </script>
    <script type="text/javascript">
        $(document).ready(function(){
        var mapOptions = {
        zoom: 4,
        center: new google.maps.LatLng(<?= $x ?>, <?= $y ?>),
        mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById('map'),mapOptions);
        var marker = new google.maps.Marker({
        position: map.getCenter(),
        map: map,
        title: '<?= $producto->titulo ?>'
        });
        });
        function resizemap()
        {
            window.open('<?= base_url('inicio/map/'.trim($x)."/".trim($y)) ?>','<?= $producto->titulo ?> - Ubicacion','width=800, height=600')
        }
    </script>
