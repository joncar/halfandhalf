<section class="content">
	<?= $output ?>
</section>
<script>
    $(document).ready(function(){
        $("#field-tipo").change(function(){
            if($(this).val()=='Intercambio'){
                
                var field = '<div id="intercambio_field_box" class="form-field-box odd">'+
                '<div id="intercambio_display_as_box" class="form-display-as-box">'+
		'A cambio de: <span class="required">*</span>  :'+
		'</div>'+
		'<div id="intercambio_input_box" class="form-input-box">'+
		'<input type="text" maxlength="255" value="" name="intercambio" id="field-intercambio"></div>'+
		'<div class="clear"></div>'+
                '</div>';
                $("#titulo_display_as_box").html('Ofrezco: <span class="required">*</span>  :');
                $("#titulo_field_box").after(field);
                $("#precio_field_box").hide();
                $("#field-precio").val('0');
                $("#field-precio").prop('readonly',true);
                }
            else if($(this).val() == 'Oferta')
                {
                    $("#titulo_display_as_box").html('Titulo: <span class="required">*</span>  :');
                    $("#intercambio_field_box").remove();
                    $("#field-precio").val('0');
                    $("#precio_field_box").hide();
                    $("#field-precio").prop('readonly',true);
                }
            else
                {
                $("#titulo_display_as_box").html('Titulo: <span class="required">*</span>  :');
                $("#intercambio_field_box").remove();
                $("#field-precio").val('');
                $("#precio_field_box").show();
                $("#field-precio").prop('readonly',false);
                }
        });
        
	var changeYear = $( "#field-fecha_nacimiento" ).datepicker( "option", "changeYear" );
	$("#field-fecha_nacimiento").datepicker( "option", "yearRange", "-60:+0" );
    });
</script>
