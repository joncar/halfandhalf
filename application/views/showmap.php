<html>
    <head>
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDBv7rAHZNUTj1V0rLfdTB7gEtcO7rc9QE&sensor=true">
    </script>
    <script type="text/javascript">
      function initialize() {
        var mapOptions = {
            zoom: 4,
            center: new google.maps.LatLng(<?= $x ?>, <?= $y ?>),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        var map = new google.maps.Map(document.getElementById('map_canvas'),
        mapOptions);

        var marker = new google.maps.Marker({
            position: map.getCenter(),
            map: map,
            title: 'Click to zoom'
        });
        }

    </script></head>
    <body>
    <div id="map_canvas" style="width:100%; height:100%"></div>
    <script>initialize();</script>

    </body>
</html>
