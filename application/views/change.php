<section>
    <div class="well" align="center">
    <? if(!empty($msj)) echo $msj ?>
    <p class="alert alert-info">Ingresa los datos para cambiar tu contraseña</p>
        <form method='post' action="<?= base_url('inicio/recover_submit') ?>">
        <h4>Datos</h4>
            <input type="password" name="pass1" data-val="required" placeholder="Nuevo Password"><br>
            <input type="password" name="pass2" data-val="required" placeholder="Repite el Nuevo Password"><br>
            <input type="hidden" name="email" data-val="required" value='<?= $email ?>'><br>
	    <p><button type="submit" class="btn btn-success btn-large">Cambiar contraseña</button></p>
    </div>
</section>