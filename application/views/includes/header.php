<!Doctype html>
<html lang="es">
	<head>
		<title>HALF & HALF - THINK BIG - FUNDACION TELEFONICA</title>
		<meta charset="utf8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<script src="<?= base_url('assets/grocery_crud/js/jquery-1.8.2.js') ?>"></script>
		<script src="<?= base_url('assets/bootstrap/js/bootstrap.min.js') ?>"></script>
		<script src="<?= base_url('assets/frame.js') ?>"></script>
		<link rel="stylesheet" type="text/css" href="<?= base_url('assets/bootstrap/css/bootstrap.min.css') ?>">
		<link rel="stylesheet" type="text/css" href="<?= base_url('assets/bootstrap/css/bootstrap-responsive.min.css') ?>">
		<script src="<?= base_url('assets/foundation/js/foundation.min.js') ?>"></script>
		<?php 
		if(!empty($css_files) && !empty($js_files)):
		foreach($css_files as $file): ?>
		<link type="text/css" rel="stylesheet" href="<?= $file ?>" />
		<?php endforeach; ?>
		<?php foreach($js_files as $file): ?>
		<script src="<?= $file ?>"></script>
		<?php endforeach; endif; ?>
		<style>
			.alert-box{
				color: white !important; font-weight: 300 !important; margin: 0px !important; padding: 8px !important;
			}
			.alert-box:hover{
				color:black !important;
			}
			.content{margin:15px;}
			.row{margin:10px; border-bottom:1px solid lightgray; padding-bottom:10px;}
		</style>
	</head>
	<body>
		<nav class="navbar navbar-inverse">
			<div class="navbar-inner">
			<div class="container">
				<a data-target=".nav-collapse" data-toggle="collapse" class="btn btn-navbar">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				</a>
				<a href="<?= site_url() ?>" class="brand">Half and Half - Fundacion Telefónica</a>
				<div class="nav-collapse collapse">
					<ul class="nav pull-right">
					<? if(empty($_SESSION['user'])): ?>
					<li><a href="javascript:getajax('<?= base_url('a/registrar') ?>')">Registrar</a></li>
					<li><a href="javascript:getajax('<?= base_url('a/conectar') ?>')">Conectar</a></li>
					<? else: ?>
                                        <li><a href="#" title="Estos son tus puntos, usalos sabiamente"><img src="<?= base_url('img/monedas.png') ?>"> <?= $this->db->get_where('user',array('id'=>$_SESSION['user']))->row()->puntos; ?></a></li>
                                        <li class="dropdown">
                                            <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="icon-white icon-user"></i> <?= $_SESSION['nombre']." ".$_SESSION['apellido'] ?> <b class="caret"></b></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="<?= base_url('inicio/perfil') ?>">Perfil</a></li>
                                                <li><a href="<?= base_url('inicio/recover/'.urlencode($_SESSION['email']).'/'.$this->db->get_where('user',array('id'=>$_SESSION['user']))->row()->activacion) ?>">Cambiar contraseña</a></li>
                                                <? if($_SESSION['cuenta']=='admin'): ?>
                                                <li><a href="<?= base_url('admin') ?>">Admin</a></li>
                                                <?endif?>
                                                <li><a href="<?= base_url('inicio/unlog') ?>">Salir</a></li>
                                                <? endif ?>
                                            </ul>
                                        </li>
					</ul>
				</div>     
			</div>
			</div>
		</nav>