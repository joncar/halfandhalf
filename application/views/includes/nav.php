		<div class="navbar">
              <div class="navbar-inner">
                <div class="container">
                  <div class="nav-collapse collapse navbar-responsive-collapse">
                    <ul class="nav">
                      <li class="active"><a href="<?= site_url() ?>"><i class="icon icon-home"></i> Inicio</a></li>
                      <li><a href="<?= site_url('favoritos') ?>"><i class="icon icon-th-list"></i> Favoritos</a></li>
                      <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="icon icon-tasks"></i> Categorias <b class="caret"></b></a>
                        <ul class="dropdown-menu">
						
                          <?
						  $primaria = array();
						  $this->db->select('catprimaria.nombre as primaria, categorias.nombre as categoria, categorias.id');
						  $this->db->join('catprimaria','categorias.primaria = catprimaria.id','inner');
						  $categorias = $this->db->get('categorias'); 
						  foreach($categorias->result() as $cat) :?>
							<? if(!in_array($cat->primaria,$primaria)): ?>
							<li><a href="#"><b><?= $cat->primaria ?></b></a></li>
							<? array_push($primaria,$cat->primaria) ?>
							<? endif ?>
							<li><a href="javascript:$('#cat').prop('value','<?= $cat->id ?>');$('#search').submit()"><?= $cat->categoria ?></a></li>
						  <? endforeach ?>
                        </ul>
                      </li>
					  <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="icon  icon-globe"></i> Ciudades <b class="caret"></b></a>
                        <ul class="dropdown-menu">
							<? foreach($this->db->get('ciudades')->result() as $ciudades): ?>
								<li><a href="javascript:$('#ciudad').prop('value','<?= $ciudades->id ?>');$('#search').submit()"><?= $ciudades->nombre ?></a></li>
							<? endforeach ?>
                        </ul>
                      </li>
                    </ul>
                    <form action="<?= site_url() ?>" id="search" method="get" class="navbar-search pull-left form-search">
					<div class="input-append">
                      <?= form_input('search','','placeholder="¿Que buscas?" class="search-query span4"') ?>
					  <? if(!empty($_GET['ciudad'])) echo '<input type="hidden" name="ciudad" value="'.$_GET['ciudad'].'" id="ciudad">'; else echo '<input type="hidden" name="ciudad" id="ciudad">' ?>
					  <? if(!empty($_GET['cat'])) echo '<input type="hidden" name="cat" value="'.$_GET['cat'].'" id="cat">'; else echo '<input type="hidden" name="cat" id="cat">'; ?>
					  <? if(!empty($_GET['order'])) echo '<input type="hidden" name="order" value="'.$_GET['order'].'" id="order">'; else echo '<input type="hidden" name="order" id="order">'; ?>
					  <button type="submit" class="btn"><i class="icon icon-search"></i> </button>
					</div>
                    </form>
                    <ul class="nav pull-right">
                      <li><a href="<?= site_url('mis-anuncios') ?>"><i class="icon icon-bell"></i> Mis anuncios</a></li>
                      <li class="divider-vertical"></li>
                      <li>
                        <a href="<?= site_url('publicar') ?>">Publica tu anuncio</a>
                      </li>
                    </ul>
                  </div><!-- /.nav-collapse -->
                </div>
              </div><!-- /navbar-inner -->
            </div>