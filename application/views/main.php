<section class="content">
				    <ul class="breadcrumb">
						<li><a href="<?= site_url() ?>">Inicio</a> <span class="divider">/</span></li>
						<? if(!empty($_GET['cat']) && $this->db->get_where('categorias',array('id'=>$_GET['cat']))->num_rows>0): ?>
						<li><a href="<?= site_url('?cat='.$_GET['cat']) ?>"><?= $this->db->get_where('categorias',array('id'=>$_GET['cat']))->row()->nombre ?></a> <span class="divider">/</span></li>
						<? endif ?>
						<? if(!empty($_GET['ciudad']) && $this->db->get_where('ciudades',array('id'=>$_GET['ciudad']))->num_rows>0): ?>
						<li><a href="<?= !empty($_GET['cat'])?site_url('?cat='.(int)$_GET['cat'].'&ciudad='.$_GET['ciudad']):site_url('?ciudad='.$_GET['ciudad']) ?>"><?= $this->db->get_where('ciudades',array('id'=>$_GET['ciudad']))->row()->nombre ?></a> <span class="divider">/</span></li>
						<? endif ?>
						
						<? if(!empty($_GET['search'])): ?>
						<li><?= $_GET['search']?></li>
						<? endif ?>
					</ul>
                                        <? $productos = $this->main->getProducts($page) ?>
    <div class="row-fluid">
        <div class="span3 well">
            <h4>Dejanos tu comentario</h4>
            <? foreach($this->db->get_where('comentarios',array('status'=>'1'))->result() as $c): ?>
            <div style="border-bottom:1px solid lightslategrey; padding:5px; margin-bottom:10px;"><b><?= $c->email ?> Dice: </b> <?= $c->comentario ?></div>
            <? endforeach ?>
            <form action="" method="post" onsubmit="return send(this,'<?= base_url('inicio/comment') ?>')">
            <input type="email" name="comment_email" placeholder="Email">
            <textarea name="comentario" placeholder="Escribe tu comentario">
            
            </textarea>
            <div align="center"><button type="submit" class="btn btn-success">Enviar</button></div>
            </form>
        </div>
					<div class="container span9">
                                            <? if($productos->num_rows>0): ?><h4>ANUNCIOS PUBLICADOS</h4><div id="map" style="width:100%; height:200px;"> mapa</div><?endif?>
					<div class="right" style="margin-right:10px"><small> Ordenar por: <a href="javascript:$('#order').prop('value','menor');$('#search').submit()">menor puntos</a> | <a href="javascript:$('#order').prop('value','mayor');$('#search').submit()">mayor puntos</a></small></div>
					<article class="well">
					
					<? if($productos->num_rows==0): ?>
						Sin resultados para mostrar, Se el primero en publicar un anuncio para esta categoria.
					<? endif ?>
					<? foreach($productos->result() as $row):  ?>
					<div class="row">
						<div class="span1"><img src="<?= base_url('assets/uploads/pictures/'.$row->foto1) ?>" class="img-polaroid"></div>
						<div class="span7">
							<h4><a href="<?= $row->url ?>"><?= empty($row->intercambio)?$row->titulo:$row->titulo." por ".$row->intercambio ?></a></h4>
							<?= $row->short_descripcion ?>
						</div>
						<div class="span2">
							<h6 style="color:red; margin-bottom:-20px"><?= $this->main->price($row->precio) ?></h6><br/>
							<small><?= $row->fecha ?></small>
						</div>
					</div>
					<? endforeach ?>
					<div class="pull-right pagination">
						<ul>
						<?= $this->main->getPagination($productos);  ?>
						</ul>
					</div>
					</article>
					</div>
    </div>
			</Section>
<? if($productos->num_rows>0): ?>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDBv7rAHZNUTj1V0rLfdTB7gEtcO7rc9QE&sensor=true"></script>
    <script type="text/javascript">
      function initialize() {
  var mapOptions = {
    zoom: 4,
    center: new google.maps.LatLng(40.245991504199026, -3.603515625),
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };

  var map = new google.maps.Map(document.getElementById('map'),
      mapOptions);
 
    <? foreach($productos->result() as $row):  ?>
        <? 
        $c = str_replace('(','',$row->coordenadas);
        $c = str_replace(')','',$c); 
        ?>
        var marker = new google.maps.Marker({position: new google.maps.LatLng(<?= $c ?>, false),map: map,title: 'Click to zoom'});
    <? endforeach ?>
}

    </script>
<script>initialize();</script>
<? endif ?>