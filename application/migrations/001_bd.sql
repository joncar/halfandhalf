-- phpMyAdmin SQL Dump
-- version 3.5.8
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 08-10-2013 a las 12:51:32
-- Versión del servidor: 5.1.70-cll
-- Versión de PHP: 5.3.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `sidopec_halfandhalf`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE IF NOT EXISTS `categorias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `primaria` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`id`, `primaria`, `nombre`) VALUES
(1, 1, 'Casas'),
(2, 1, 'Oficinas'),
(3, 1, 'Apartamentos'),
(4, 1, 'Comercial, Industrial'),
(5, 1, 'Terrenos'),
(6, 1, 'Otras propiedades'),
(7, 2, 'Carros'),
(8, 2, 'Camionetas'),
(9, 2, 'Respuestos'),
(10, 2, 'Barcos'),
(11, 3, 'Electrodomesticos'),
(12, 3, 'Articulos de interior'),
(13, 3, 'Niños'),
(14, 3, 'Jardin'),
(15, 3, 'Ropa y accesorios'),
(16, 4, 'Hobby'),
(17, 4, 'Deportes y bicicletas'),
(18, 4, 'Peliculas y Libros'),
(19, 4, 'Animales y Mascotas'),
(20, 5, 'Celulares y telefonos'),
(21, 5, 'Tv, Audio, Video y Camaras'),
(22, 5, 'Computación y accesorios'),
(23, 5, 'Consola y Videojuegos'),
(24, 6, 'Trabajos'),
(25, 6, 'Curriculums');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `catprimaria`
--

CREATE TABLE IF NOT EXISTS `catprimaria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `catprimaria`
--

INSERT INTO `catprimaria` (`id`, `nombre`) VALUES
(1, 'Inmobiliaria'),
(2, 'Carros, Motos y Barcos'),
(3, 'Hogar y Personal'),
(4, 'Deportes y Pasatiempos'),
(5, 'Electronica'),
(6, 'Negocio y Trabajos');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ciudades`
--

CREATE TABLE IF NOT EXISTS `ciudades` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Volcado de datos para la tabla `ciudades`
--

INSERT INTO `ciudades` (`id`, `nombre`) VALUES
(1, 'Castilla-La Mancha'),
(2, 'Andalucia'),
(3, 'Madrid'),
(4, 'Valencia'),
(5, 'Castilla y León'),
(6, 'Asturias'),
(7, 'Extremadura'),
(9, 'Cataluña'),
(10, 'País Vasco'),
(11, 'Murcia'),
(12, 'Ceuta'),
(13, 'Galicia'),
(14, 'Aragón'),
(15, 'Canarias'),
(16, 'La Rioja'),
(17, 'Melilla'),
(18, 'Baleares'),
(19, 'Navarra'),
(20, 'Cantabria');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `favoritos`
--

CREATE TABLE IF NOT EXISTS `favoritos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `producto` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `preguntas`
--

CREATE TABLE IF NOT EXISTS `preguntas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `producto` int(11) NOT NULL,
  `texto` varchar(255) NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE IF NOT EXISTS `productos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `categoria` int(11) NOT NULL,
  `tipo` varchar(255) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `descripcion` text NOT NULL,
  `precio` decimal(9,2) NOT NULL,
  `ciudad` int(11) NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `foto1` varchar(255) NOT NULL,
  `foto2` varchar(255) NOT NULL,
  `foto3` varchar(255) NOT NULL,
  `foto4` varchar(255) NOT NULL,
  `foto5` varchar(255) NOT NULL,
  `foto6` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `respuestas`
--

CREATE TABLE IF NOT EXISTS `respuestas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pregunta` int(11) NOT NULL,
  `texto` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipos`
--

CREATE TABLE IF NOT EXISTS `tipos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `apellido` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `telefono` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `cuenta` varchar(255) NOT NULL,
  `activacion` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`id`, `nombre`, `apellido`, `email`, `password`, `telefono`, `status`, `fecha`, `cuenta`, `activacion`) VALUES
(1, 'Administrator', '', 'admin@halfandhalf.es', '25d55ad283aa400af464c76d713c07ad', '', 1, '2013-10-04', 'admin', '74bba22728b6185eec06286af6bec36d'),
(2, 'Jonathan', 'Cardozo', 'joncar.c@gmail.com', '25d55ad283aa400af464c76d713c07ad', '212123123', 1, '2013-10-07', 'free', 'd395771085aab05244a4fb8fd91bf4ee');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
