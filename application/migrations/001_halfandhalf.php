<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Halfandhalf extends CI_Migration {

	public function up()
	{
		 //read the file
		$file = $this->load->file('application/migrations/001_bd.sql', true);

		//explode it in an array
		$file_array = explode(';', $file);

		//execute the exploded text content
		foreach($file_array as $query)
		$this->db->query($query);  
	}

	public function down()
	{
		$this->dbforge->drop_table('productos');
		$this->dbforge->drop_table('user');
		$this->dbforge->drop_table('tipos');
		$this->dbforge->drop_table('respuestas');
		$this->dbforge->drop_table('favoritos');
		$this->dbforge->drop_table('ciudades');
		$this->dbforge->drop_table('catprimaria');
		$this->dbforge->drop_table('categorias');
		
		
	}
}
?>