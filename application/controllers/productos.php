<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once("inicio.php");

class Productos extends Inicio{
 	public function __construct()
	{
		parent::__construct();
	}
	
	function publicar($action = 'list',$data = '')
	{
                if($action=='edit')
                {
                    $p = $this->db->get_where('productos',array('id'=>$data));
                    if($p->num_rows == 0 || $p->row()->user!=$_SESSION['user']);
                    $this->loadView('404');
                    return false;
                }
		
		try{
		$crud = new grocery_CRUD();
		$crud->set_theme('flexigrid');
		$crud->set_table('productos');
		$crud->set_subject('<i class="icon icon-bell"></i> Mis Anuncios');
		$crud->set_relation('user','user','nombre');
		$crud->set_relation('categoria','categorias','nombre');
		$crud->set_relation('tipo','tipos','nombre');
		$crud->set_relation('ciudad','ciudades','nombre');
		$crud->columns('titulo','descripcion','Preguntas');
		$crud->callback_column('titulo',array($this,'linkproduct'));
                $crud->callback_add_field('coordenadas',array($this,'map'));
		//$crud->unset_edit();
		$crud->unset_delete();
		for($i=1;$i<=6;$i++)
		$crud->set_field_upload('foto'.$i,$this->pathPictures);
		
		if(!empty($_SESSION['user']))$crud->where('user',$_SESSION['user']);
		$crud->field_type('user','invisible');
		$crud->field_type('fecha','invisible');
                $crud->field_type('intercambio','invisible');
		$crud->field_type('tipo','enum',array('Oferta','Demanda','Intercambio','Oferta de empleo'));
                $crud->display_as('precio','Puntos');
                $crud->display_as('descripcion','Descripcion del servicio');
		$crud->required_fields('categoria','tipo','titulo','descripcion','precio','ciudad','localidad','coordenadas');
                if(!empty($_POST) && $_POST['tipo']!='Intercambio' && $_POST['tipo']!='Oferta')
                $crud->set_rules('precio','Puntos','required|numeric|greater_than[0]|less_than['.$this->db->get_where('user',array('id'=>$_SESSION['user']))->row()->puntos.']');
		$crud->callback_before_insert(array($this,'binsertion'));
                $crud->callback_column('Preguntas',array($this,'countpreguntas'));
		$output = $crud->render($action);
		$output->view = 'publicar';
		$this->loadView($output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}		
	}
	
	function countpreguntas($val,$row)
	{
		$m = $this->db->get_where('preguntas',array('producto'=>$row->id))->num_rows;
		return ($m>0)?'<a class="badge badge-info" href="'.base_url('productos/respuestas/'.$row->id).'">'.$m.'</a>':'<span class="badge">'.$m.'</span>';
	}
        
        function map()
        {
            return $this->load->view('mapform',null,TRUE);
        }
	
	function preguntas($action)
	{
		$crud = new grocery_CRUD();
		$crud->set_theme('flexigrid');
		$crud->set_table('preguntas');
		$crud->set_subject('Preguntas');
		
		$crud->field_type('user','invisible');
		$crud->callback_before_insert(array($this,'binsertion'));
		$crud->required_fields('texto');
		
		$output = $crud->render($action);
		$output->view = 'publicar';
		$this->loadViewAjax($output);
	}
	
	function favoritos($action = null)
	{
		$crud = new grocery_CRUD();
		$crud->set_theme('flexigrid');
		$crud->set_table('favoritos');
		$crud->set_subject('<i class="icon icon-th-list"></i> Favoritos');
		$crud->columns('producto');
		
		//$crud->set_relation('producto','productos','titulo');
		$crud->callback_column('producto',array($this,'linkproductfav'));
		$crud->unset_edit();
		$crud->field_type('user','invisible');
		$crud->callback_before_insert(array($this,'binsertion'));
		$crud->required_fields('producto');
		
		$output = $crud->render($action);
		$output->view = 'publicar';
		$this->loadView($output);
	}
	
	function respuestas($id = 0)
	{
		if(is_numeric($id) && $id>0)
		{
		$crud = new grocery_CRUD();
		$crud->set_theme('flexigrid');
		$crud->set_table('preguntas');
		$crud->set_subject('Respuestas');
		$crud->set_relation('user','user','nombre');
		$crud->set_relation('producto','productos','titulo');
		$crud->columns('user','producto','fecha','texto','responder');
		$crud->callback_column('responder',array($this,'responderField'));
		$crud->display_as('texto','Pregunta');
		$crud->where('producto',$id);
		$crud->field_type('pregunta','hidden',$id);
		$crud->required_fields('texto');
		$crud->unset_add();
		$crud->unset_edit();
		$crud->unset_delete();
		$output = $crud->render();
		$output->view = 'publicar';
		$this->loadView($output);
		}
		else
		$this->loadView('404');
	}
	
	function responder($action)
	{
		$crud = new grocery_CRUD();
		$crud->set_theme('flexigrid');
		$crud->set_table('respuestas');
		$crud->set_subject('Respuestas');
		$crud->required_fields('texto');
		$output = $crud->render($action);
		$output->view = 'publicar';
		$this->loadView($output);
	}
	
	function responderField($val,$row)
	{
		$r = $this->db->get_where('respuestas',array('pregunta'=>$row->id));
		if($r->num_rows==0){
		$x = form_open('responder'.$row->id,'onsubmit="return send(this,\''.base_url('productos/responder/insert').'\')"');
		$x.= form_hidden('pregunta',$row->id,'id="field-pregunta"');
		$x.= form_input('texto','','id="field-texto" placeholder="Responder"');
		$x.= form_close();
		}
		else $x = $r->row()->texto;
		return $x;
	}
	
	function view($id)
	{
		$id = explode('-',$id);
		$id = $id[count($id)-1];
		$producto = $this->main->getProduct($id);
		$view = (!$producto)?$this->errorView:'watch';
		
		$this->loadView(array('view'=>$view,'producto'=>$producto));
	}
	
	function linkproduct($val,$row)
	{
		$url = site_url('watch/'.urlencode($val)."-".$row->id);
		$url = str_replace('+','-',$url);
		return '<a href="'.$url.'">'.$val.'</a>';
	}
	
	function linkproductfav($val,$row)
	{
		$x = $this->db->get_where('productos',array('id'=>$val))->row();
		$url = site_url('watch/'.urlencode($x->titulo)."-".$x->id);
		$url = str_replace('+','-',$url);
		return '<a href="'.$url.'">'.$x->titulo.'</a>';
	}
	
	function binsertion($post)
	{
		$post['user'] = $this->adduser();
		$post['fecha'] = date("Y-m-d H:i:s");
                $puntos = $this->db->get_where('user',array('id'=>$_SESSION['user']))->row()->puntos;
                $puntos = $puntos-$post['precio'];
                if($puntos<0)$puntos = 0;
                $this->db->where('id',$_SESSION['user']);
                $this->db->update('user',array('puntos'=>$puntos));
		return $post;
	}
	
}