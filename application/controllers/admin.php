<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once("inicio.php");

class Admin extends Inicio{
 	public function __construct()
	{
		parent::__construct();
		if($_SESSION['cuenta']!='admin')
		header("Location:".site_url());
	}
	
	function index()
	{
		$this->loadView('admin');
	}
	
	function ciudades()
	{
		$crud = new grocery_CRUD();
		$crud->set_theme('admin');
		$crud->set_table('ciudades');
		$crud->set_subject('Ciudades');
		//$crud->unset_delete();
		$crud->required_fields('nombre');
        
		$output = $crud->render();
		$output->view = 'admin';
		$this->loadView($output);
	}
	
	function catprimaria()
	{
		$crud = new grocery_CRUD();
		$crud->set_theme('admin');
		$crud->set_table('catprimaria');
		$crud->set_subject('Categorias Primarias');
		$crud->required_fields('nombre');
		//$crud->unset_delete();
		$output = $crud->render();
		$output->view = 'admin';
		$this->loadView($output);
	}	
	
	function usuarios()
	{
		$crud = new grocery_CRUD();
		$crud->set_theme('admin');
		$crud->set_table('user');
		$crud->set_subject('Usuarios');
		$crud->unset_add();
		//$crud->unset_delete();
                $crud->required_fields('nombre','apellido','cuenta','puntos');
                $crud->set_rules('puntos','Puntos','required|numeric');
                $crud->edit_fields('puntos','status','cuenta');
                $crud->field_type('email','readonly');
                $crud->field_type('fecha','readonly');
                $crud->field_type('status','enum',array('0','1'));
                $crud->field_type('cuenta','enum',array('admin','free'));
                $crud->display_as('fecha','Fecha de registro');
                $crud->unset_fields('password');
                $crud->unset_columns('activacion','password');
		$output = $crud->render();
		$output->view = 'admin';
		$this->loadView($output);
	}
        
        function anuncios()
	{
		try{
		$crud = new grocery_CRUD();
		$crud->set_theme('flexigrid');
		$crud->set_table('productos');
		$crud->set_subject('<i class="icon icon-bell"></i> Mis Anuncios');
		$crud->set_relation('user','user','nombre');
		$crud->set_relation('categoria','categorias','nombre');
		$crud->set_relation('tipo','tipos','nombre');
		$crud->set_relation('ciudad','ciudades','nombre');
		$crud->columns('user','titulo','descripcion','Preguntas');
		$crud->callback_column('titulo',array($this,'linkproduct'));
                $crud->callback_add_field('coordenadas',array($this,'map'));
		//$crud->unset_edit();
		//$crud->unset_delete();
		for($i=1;$i<=6;$i++)
		$crud->set_field_upload('foto'.$i,$this->pathPictures);
		$crud->field_type('user','invisible');
		$crud->field_type('fecha','invisible');
                $crud->field_type('intercambio','invisible');
		$crud->field_type('tipo','enum',array('Oferta','Demanda','Intercambio','Oferta de empleo'));
                $crud->display_as('precio','Puntos');
                $crud->display_as('descripcion','Descripcion del servicio');
		$crud->required_fields('categoria','tipo','titulo','descripcion','precio','ciudad','localidad','coordenadas');
                if(!empty($_POST) && $_POST['tipo']!='Intercambio' && $_POST['tipo']!='Oferta')
                $crud->set_rules('precio','Puntos','required|numeric|greater_than[0]|less_than['.$this->db->get_where('user',array('id'=>$_SESSION['user']))->row()->puntos.']');
		$crud->callback_before_insert(array($this,'binsertion'));
                $crud->callback_column('Preguntas',array($this,'countpreguntas'));
		$output = $crud->render();
		$output->view = 'publicar';
		$this->loadView($output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}	
	}	
        
        function comentarios()
        {
                $crud = new grocery_CRUD();
		$crud->set_theme('admin');
		$crud->set_table('comentarios');
		$crud->set_subject('Comentarios');
		$crud->unset_add();
                $crud->unset_edit();
                $crud->unset_export();
                $crud->unset_print();
                $crud->callback_column('status',array($this,'commentstatuscolumn'));
                
		//$crud->unset_delete();
		$output = $crud->render();
		$output->view = 'admin';
		$this->loadView($output);
        }
	
	function categorias()
	{
		$crud = new grocery_CRUD();
		$crud->set_theme('admin');
		$crud->set_table('categorias');
		$crud->set_subject('Sub-categorias');
		$crud->set_relation('primaria','catprimaria','nombre');
		$crud->required_fields('primaria','nombre');
		//$crud->unset_delete();
		$output = $crud->render();
		$output->view = 'admin';
		$this->loadView($output);
	}
        
        function aprobar($id)
        {
            $this->db->update('comentarios',array('status'=>'1'));
        }
        
        
        function commentstatuscolumn($val,$row)
        {
            return $val==0?'<a href="javascript:ajax(\'\',undefined,undefined,function(){document.location.href=\''.base_url('admin/comentarios').'\'},\''.base_url('admin/aprobar/'.$row->id).'\')">Aprobar</a>':'Aprobado';
        }
        
        function countpreguntas($val,$row)
	{
		$m = $this->db->get_where('preguntas',array('producto'=>$row->id))->num_rows;
		return ($m>0)?'<a class="badge badge-info" href="'.base_url('productos/respuestas/'.$row->id).'">'.$m.'</a>':'<span class="badge">'.$m.'</span>';
	}
        
        	function linkproduct($val,$row)
	{
		$url = site_url('watch/'.urlencode($val)."-".$row->id);
		$url = str_replace('+','-',$url);
		return '<a href="'.$url.'">'.$val.'</a>';
	}
	
	function linkproductfav($val,$row)
	{
		$x = $this->db->get_where('productos',array('id'=>$val))->row();
		$url = site_url('watch/'.urlencode($x->titulo)."-".$x->id);
		$url = str_replace('+','-',$url);
		return '<a href="'.$url.'">'.$x->titulo.'</a>';
	}
	
	function binsertion($post)
	{
		$post['user'] = $this->adduser();
		$post['fecha'] = date("Y-m-d H:i:s");
                $puntos = $this->db->get_where('user',array('id'=>$_SESSION['user']))->row()->puntos;
                $puntos = $puntos-$post['precio'];
                if($puntos<0)$puntos = 0;
                $this->db->where('id',$_SESSION['user']);
                $this->db->update('user',array('puntos'=>$puntos));
		return $post;
	}
}