<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once("inicio.php");
class Registrar extends Inicio{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */   
	
 	public function __construct()
	{
		parent::__construct();
	}
	public function reg()
	{
		$this->form_validation->set_rules('nombre','Nombre','required');
		$this->form_validation->set_rules('apellido','Apellido','required');
		$this->form_validation->set_rules('email','Email','required|valid_email');
		$this->form_validation->set_rules('password','Password','required|min_length[8]');
		$this->form_validation->set_rules('password2','Password2','required|min_length[8]');
		if($this->form_validation->run())
		{
			if($this->input->post('password') == $this->input->post('password2'))
			{
				if($this->db->get_where('user',array('email'=>$this->input->post('email',TRUE)))->num_rows==0)
				{
				$data['nombre'] = $this->input->post('nombre',TRUE);
				$data['apellido'] = $this->input->post('apellido',TRUE);
				$data['email'] = $this->input->post('email',TRUE);
				$data['password'] = md5($this->input->post('password',TRUE));
				$data['telefono'] = $this->input->post('telefono',TRUE);
				$data['fecha'] = date("Y-m-d");
				$data['status'] = 0;
				$data['cuenta'] = 'free';
				$data['activacion'] = md5(rand(0,1000));
				$this->db->insert('user',$data);
				mail_activate($data['email'],$data['nombre'],$data['activacion']);
				echo $this->success('Usuario registrado con exito, Por favor verifica tu email para activar tu cuenta.');
				}
				else
				echo $this->error('Usuario ya se encuentra registrado, intenté con otro.');
			}
			else
			echo $this->error('Las contraseñas deben ser iguales');
		}
		else
		echo $this->error($this->form_validation->error_string());
	}
	
	public function activate($email,$codigo)
	{
		$email = str_replace('%40','@',$email);
		$this->db->where('email',$email);
		$this->db->where('activacion',$codigo);
		
		if($this->db->update('user',array('status'=>1)))
			$this->loadView(array('view'=>'activacion','msj'=>$this->success('La activación ha sido completada.')));
		else
			$this->loadView(array('view'=>'activacion','msj'=>$this->error('Ha habido un error activando su cuenta, comuniquese con nosotros en el modulo de contactenos')));
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */