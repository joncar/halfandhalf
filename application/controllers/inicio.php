<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ob_start();
session_start();
class Inicio extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */   
	var $rules_admin = array('all');
	var $rules_user = array('all');
	var $rules_guest = array('main','registrar','watch','forget','change');
	private $restricted = 'conectar';
	var $errorView = '404';
	var $pathPictures = 'assets/uploads/pictures';
        var $pathAvatars = 'assets/uploads/pictures';
        var $pathCurriculos = 'assets/uploads/pictures';
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('url_extend');
		$this->load->helper('form');
		$this->load->helper('h');
		$this->load->database();
		$this->load->model('user');
        $this->load->model('main');
		$this->load->library('form_validation');
		$this->load->library('grocery_crud');
		$this->load->library('pagination');
	}
	public function index($url = 'main',$page = 0)
	{
		$this->loadView(array('view'=>$url,'page'=>$page));
	}
        
        public function perfil()
        {
            $crud = new grocery_CRUD();
            $crud->set_theme('perfil');
            $crud->set_table('perfiles');
            $crud->set_subject('Perfil');
            $crud->field_type('user','invisible');
            $crud->callback_before_insert(array($this,'binsertionperfil'));
            if($this->db->get_where('perfiles',array('user'=>$_SESSION['user']))->num_rows>0)
            $crud->unset_add();
            $crud->where('user',$_SESSION['user']);
            $crud->set_field_upload('foto',$this->pathAvatars);
            $crud->set_field_upload('curriculum',$this->pathCurriculos);
            $output = $crud->render();
            $output->view = 'publicar';
            $this->loadView($output);
        }
        
        public function binsertionperfil($post)
        {
            $post['user'] = $this->adduser();
            return $post;
        }
	
        public function comment()
        {
            $this->form_validation->set_rules('comment_email','Email','required|valid_email');
            $this->form_validation->set_rules('comentario','Comentario','required|max_length[255]');
            if($this->form_validation->run())
            {
                $this->db->insert('comentarios',array('fecha'=>date("Y-m-d"),'email'=>$this->input->post('comment_email',TRUE),'comentario'=>$this->input->post('comentario',TRUE)));
                echo $this->success('Comentario enviado con éxito, Gracias...');
            }
            else
                echo $this->error($this->form_validation->string_error());
        }


	public function success($msj)
	{
		return '<div class="alert alert-success">'.$msj.'</div>';
	}

	public function error($msj)
	{
		return '<div class="alert alert-danger">'.$msj.'</div>';
	}

	public function login()
	{
		if(!$this->user->log)
		{	
			if(!empty($_POST['email']) && !empty($_POST['password']))
			{
				$this->db->where('email',$this->input->post('email'));
				$r = $this->db->get('user');
				if($this->user->login($this->input->post('email',TRUE),$this->input->post('password',TRUE)))
				{
					if($r->num_rows>0 && $r->row()->status==1)
					{
                        if(empty($_POST['redirect']))
                        echo $this->success('Usuario logueado correctamente por favor espere...! <script>document.location.href="'.site_url().'"</script>');
                        else
                        echo $this->success('Usuario logueado correctamente por favor espere...! <script>document.location.href="'.$_POST['redirect'].'"</script>');
					}
					else echo $this->error('El usuario se encuentra bloqueado, comuniquese con un administrador para solucionar su problema');
				}
				else echo $this->error('Usuario o contrasena incorrecta, intente de nuevo.');
			}
			else
				echo $this->error('Debe completar todos los campos antes de continuar');
		}
	}

	public function unlog()
	{
		$this->user->unlog();
                header("Location:".site_url());
	}
        
        public function sell($id_campana)
        {
            if(!empty($_SESSION['user']))
            {
            $data = array('id_campana'=>$id_campana,'user'=>$_SESSION['user'],'Fecha'=>date("Y-m-d H:i:s"));
            $this->db->insert('ventas',$data);
            echo $this->success("Producto agregado al carrito de compra.");
            }
            else
            {
                $this->load->view('acceso_directo',array('redirect'=>base_url('show/v/'.$id_campana),'msj'=>''));
            }
        }
        
        public function loadView($param = array('view'=>'main'))
        {
			if(is_string($param))
			$param = array('view'=>$param);
			else if(is_array($param))$param['view'] = $this->valid_rules($param['view']);
			else $param->view = $this->valid_rules($param->view);
            $this->load->view('template',$param);
        }
		
		public function loadViewAjax($view,$data = null)
        {
			$view = $this->valid_rules($view);
            $this->load->view($view,$data);
        }
	
		public function adduser()
		{
		return $_SESSION['user'];
		}
	
		public function valid_rules($request) //Methodo para validar si el acceso es autorizado
		{
			if(!empty($_SESSION['user']))
				$type = $_SESSION['cuenta'];
			else
				$type = "guest";
		
			switch($type)
			{
				case 'admin': $rules = $this->rules_admin; break;
				case 'free': $rules = $this->rules_user; break;
				case 'guest': $rules = $this->rules_guest; break;
			}
			
			if(in_array($request,$rules) || in_array('all',$rules)){
				return $request;
			}
		
			else
			return $this->restricted;
		}
                
                function forget()
                {
                    $this->form_validation->set_rules('email','Email','required|valid_email');
                    if($this->form_validation->run())
                    {
                        $email = $this->input->post('email',TRUE);
                        $user = $this->db->get_where('user',array('email'=>$email));
                        if($user->num_rows>0)
                        {
                            mail_recover($email,$user->row()->activacion);
                            $this->loadViewAjax('forget',array('msj'=>$this->success('Los pasos para reestablecer su contraseña han sido enviados a su correo electronico')));
                        }
                        else
                            $this->loadViewAjax('forget',array('msj'=>$this->error('Usuario no encontrado en base de datos.')));
                    }
                    else{
                    
                    $msj = ($this->form_validation->error_string())?$this->error($this->form_validation->error_string()):'';    
                    $this->loadViewAjax('forget',array('msj'=>$msj));
                    }
                }
                
                function recover($email,$activacion)
                {
                    $email = urldecode($email);
                    $user = $this->db->get_where('user',array('email'=>$email,'activacion'=>$activacion));
                    if(!empty($email) && !empty($activacion) && $user->num_rows>0)
                    {
                        $this->loadView(array('view'=>'change','email'=>$email));
                    }
                    else
                    $this->loadView('404');
                }
                
                function recover_submit()
                {
                    $this->form_validation->set_rules('pass1','Password','required|min_length[8]');
                    $this->form_validation->set_rules('pass2','Password','required|min_length[8]');
                    $this->form_validation->set_rules('email','Email','required|valid_email');
                    if($this->form_validation->run())
                    {
                       $pass = $this->input->post('pass1',TRUE);
                       $pass2 = $this->input->post('pass2',TRUE);
                       $email = $this->input->post('email',TRUE);
                       if($pass == $pass2)
                       {
                           $this->db->where('email',$email);
                           $this->db->update('user',array('password'=>md5($pass)));
                           $this->user->login($email,$pass);
                           header("Location:".site_url());
                       }
                       else
                           $this->loadView(array('view'=>'change','email'=>$email,'msj'=>$this->error('Las contraseñas deben ser iguales')));
                    }
                    else
                        $this->loadView(array('view'=>'change','msj'=>$this->error($this->form_validation->error_string())));
                }
                
                function map($x,$y)
                {
                    $this->load->view('showmap',array('x'=>$x,'y'=>$y));
                }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */