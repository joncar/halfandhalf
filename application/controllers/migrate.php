<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Migrate extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('migration');
		$this->load->helper('url');
		if (!$this->migration->current())
			echo show_error($this->migration->error_string());
		
		echo 'Instalación Correcta. Ya puede comenzar a utilizar HalfandHalf';
	}

}