<?php

class Main extends CI_Model{
	private $limit_page = 10;
    function __construct() {
        parent::__construct();
    }
    //Añade una clausula a las consultas
    public function where($field,$val)
    {
        $this->db->where($field,$val);
    }
	
    //Transforma de numero a moneda
    public function price($val)
    {
	//return number_format($val,2,',','.').' '.$this->db->get('ajustes')->row()->moneda;
	//number_format($val,2,',','.').' puntos';
        return (int)$val.' puntos';
    }
	public function price_avr()
    {
		//return $this->db->get('ajustes')->row()->monedan;
    }
    //Añade el simbolo de porcentaje
    public function percent($val)
    {
	return $val.' %';
    }

	public function getAllProducts($page = 0)
	{
		if(!empty($_GET['search']))
			$this->db->like('titulo',$_GET['search']);
			
		if(!empty($_GET['cat']))
			$this->db->where('categoria',$_GET['cat']);
			
		if(!empty($_GET['ciudad']))
			$this->db->where('ciudad',$_GET['ciudad']);
		
		if(!empty($_GET['order']))
			($_GET['order']=='mayor')?$this->db->order_by('precio','DESC'):$this->db->order_by('precio','ASC');
		else
		$this->db->order_by('fecha','DESC');
		$r = $this->db->get('productos');
		for($i=0;$i<$r->num_rows;$i++)
		{
		$row = $r->row($i);
		$r->row($i)->short_descripcion = strip_tags(substr($row->descripcion,0,100)."...");
		$r->row($i)->url = site_url('watch/'.urlencode($row->titulo)."-".$row->id);
		$r->row($i)->url = str_replace('+','-',$r->row($i)->url);
		if(empty($row->foto1))
			$r->row($i)->foto1 = 'vacio.png';
		$r->row($i)->fecha = $this->transform_date($row->fecha);
		}
		return $r;
	}
	
	function getProducts($page = 0)
	{
		$this->db->limit($this->limit_page,$page);
		return $this->getAllProducts($page);
	}
	
	public function transform_date($date)
	{
		if(function_exists ('date_diff')){
		$interval = date_diff(date_create('now'), date_create($date));
		if($interval->format('%a')>30)
			$result = 'Publicado el: '.date("d-m-Y",strtotime($date));
		else if($interval->format('%a')>1)
			$result = 'Publicado hace: '.$interval->format('%a Dias');
		else if($interval->format('%a')==1)		
			$result = 'Publicado hace: '.$interval->format('%a Dia');
		else if($interval->format('%i')>1) 
			$result = 'Publicado hace: '.$interval->format('%i Minutos');
		else if($interval->format('%i') == 1)
			$result = 'Publicado hace: '.$interval->format('%i Minuto');
			
		else if($interval->format('%s') > 1)
			$result = 'Publicado hace: '.$interval->format('%s Segundos');
		}
		else
		$result = date("d/m/Y",strtotime($date));
		return $result;
	}
	
	public function getProduct($id = 0)
	{
		$this->db->select('productos.*,categorias.nombre as cat,ciudades.nombre as ciud,user.nombre as Nombre,user.apellido as Apellido,user.telefono,user.email');
		$this->db->join('categorias','productos.categoria = categorias.id','inner');
		$this->db->join('ciudades','productos.ciudad = ciudades.id','inner');
		$this->db->join('user','productos.user = user.id','inner');
		$this->db->order_by('fecha','ASC');
		$r = $this->db->get_where('productos',array('productos.id'=>$id))->row();
		$r->preguntas = $this->getPreguntas($id);
		return $r;
	}
	
	public function getPreguntas($producto)
	{
		$this->db->order_by('fecha','DESC');
		return $this->db->get_where('preguntas',array('producto'=>$producto));
	}
	
	public function getRespuestas($pregunta)
	{
		return $this->db->get_where('respuestas',array('pregunta'=>$pregunta))->row();
	}
	
	public function getPagination($row)
	{
		$search = (!empty($_GET['search']))?'search='.$_GET['search']:'search=';
		$cat =(!empty($_GET['cat']))?'&cat='.$_GET['cat']:'&cat=';
		$ciudad = (!empty($_GET['ciudad']))?'&ciudad='.$_GET['ciudad']:'&ciudad=';
		$order = (!empty($_GET['order']))?'&order='.$_GET['order']:'&order=';
		$config['first_link'] = 'FIRST';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#"><b>';
		$config['cur_tag_close'] = '</b></a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		
		$config['first_url'] = base_url().'?'.$search.$cat.$ciudad.$order;
		$config['suffix'] = '?'.$search.$cat.$ciudad.$order;
		$config['base_url'] = base_url('page');
		$config["uri_segment"] = 2;
		$config['total_rows'] = $this->getAllProducts()->num_rows;
		$config['per_page'] = $this->limit_page;
		$this->pagination->initialize($config);
		return $this->pagination->create_links();
	}
}
?>
